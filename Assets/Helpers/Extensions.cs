using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions {

    public static Element RandomElement<Element>(this List<Element> list) {
        return list[Random.Range(0, list.Count - 1)];
    }
    
    public static Element RandomElement<Element>(this Element[] array) {
        return array[Random.Range(0, array.Length - 1)];
    }

    public static float RandomFloat(this System.Random random, float min, float max) {
        return (float)(random.NextDouble() * ((double)max - (double)min) + (double)min);
    }
}
