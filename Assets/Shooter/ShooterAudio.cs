using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ShooterAudio : MonoBehaviour {

    [SerializeField] private AudioClip chargeClip;
    [SerializeField] private AudioClip fireClip;

    private AudioSource audioSource;

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayChargeClip() {
        audioSource.Stop();
        audioSource.PlayOneShot(chargeClip);
    }

    public void PlayFireClip() {
        audioSource.Stop();
        audioSource.PlayOneShot(fireClip);
    }
}
