using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AmmoHandler: MonoBehaviour {

    [SerializeField] private int maxAmmo = 10;

    private int currentAmmo;
    public int CurrentAmmo { get { return currentAmmo; } }

    private ShootingPowerHandler shootingPowerHandler;

    public event Action OnCurrentAmmoChanged;

    private void OnDestroy() {
        OnCurrentAmmoChanged = null;
    }

    private void Awake() {
        currentAmmo = maxAmmo;
    }

    private void Start() {
        shootingPowerHandler = GetComponent<ShootingPowerHandler>();
        shootingPowerHandler.OnShoot += DecrementAmmo;
    }

    private void DecrementAmmo() {
        currentAmmo--;
        OnCurrentAmmoChanged?.Invoke();
    }
}
