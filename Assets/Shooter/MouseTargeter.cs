using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class MouseTargeter: MonoBehaviour {

    [SerializeField] private InputAction input;
    [SerializeField] private Camera targetCamera;
    [SerializeField] private float yAimModifier;

    private void OnEnable() {
        input.Enable();
    }

    private void OnDisable() {
        input.Disable();
    }

    private void Update() {
        LookAtMousePosition();
    }

    private void LookAtMousePosition() {
        Vector2 position = input.ReadValue<Vector2>();
        Ray ray = targetCamera.ScreenPointToRay(new Vector3(position.x, position.y, 0f));
        if (Physics.Raycast(targetCamera.transform.position, ray.direction, out RaycastHit hitInfo, float.MaxValue, LayerMask.GetMask(Constants.Layers.targetLayer))) {
            transform.LookAt(new Vector3(hitInfo.point.x, hitInfo.point.y + yAimModifier, hitInfo.point.z));
        }
    }
}
