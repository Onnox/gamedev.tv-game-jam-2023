using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

[RequireComponent(typeof(AmmoHandler))]
[RequireComponent(typeof(ShooterAudio))]
public class ShootingPowerHandler: MonoBehaviour {
    
    [SerializeField] private InputAction input;
    [SerializeField] private float powerGainSpeed = 0.5f;
    private bool hasStartedShooting = false;
    private float maxPower = 1f;
    [Space]
    [SerializeField] private ParticleSystem fireParticles;
    
    private float currentPower = 0f;
    public float CurrentPower { get { return currentPower; } }

    private AmmoHandler ammoLimit;
    private ShooterAudio shooterAudio;

    private bool CanShoot { 
        get { 
            if (ammoLimit == null) {
                return true;
            } else {
                return ammoLimit.CurrentAmmo > 0;
            }
        }
    }

    public event Action OnPowerChanged;
    public event Action OnShoot;

    private void Awake() {
        ammoLimit = GetComponent<AmmoHandler>();
        shooterAudio = GetComponent<ShooterAudio>();
    }

    private void OnDestroy() {
        OnPowerChanged = null;
        OnShoot = null;
    }

    private void OnEnable() {
        input.Enable();
    }

    private void OnDisable() {
        input.Disable();
    }

    private void FixedUpdate() {
        float buttonPress = input.ReadValue<float>();
        if (buttonPress > 0 && CanShoot) {
            HandleShootButtonIsPressed();
        } else {
            HandleShootButtonIsReleased();
        }
    }

    private void HandleShootButtonIsPressed() {
        if (!hasStartedShooting) {
            hasStartedShooting = true;
            shooterAudio.PlayChargeClip();
        }
        currentPower += powerGainSpeed * Time.fixedDeltaTime;

        if (currentPower > maxPower) {
            currentPower = maxPower;
        }

        OnPowerChanged?.Invoke();
    }

    private void HandleShootButtonIsReleased() {
        if (hasStartedShooting) {
            hasStartedShooting = false;
            OnShoot?.Invoke();
            currentPower = 0f;
            OnPowerChanged?.Invoke();
            fireParticles.Play();
            shooterAudio.PlayFireClip();
        }
    }
}
