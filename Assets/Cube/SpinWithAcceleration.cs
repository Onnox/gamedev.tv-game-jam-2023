using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinWithAcceleration : MonoBehaviour {

    [SerializeField] private float initialSpinSpeed = 1f;
    [SerializeField] private float spinAcceleration = 1.01f;

    private float currentSpinSpeed = 0f;

    private void Start() {
        currentSpinSpeed = initialSpinSpeed;
    }

    private void FixedUpdate() {
        transform.Rotate(0f, currentSpinSpeed, 0f);
        currentSpinSpeed *= spinAcceleration;
    }
}
