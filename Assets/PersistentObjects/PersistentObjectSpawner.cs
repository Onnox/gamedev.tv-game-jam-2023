using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentObjectSpawner : MonoBehaviour {

    [Tooltip("This prefab will only be spawned once and persisted between scenes.")]
    [SerializeField] private GameObject persistentObjectPrefab = null;

    private static bool hasSpawned = false;

    void Awake() {
        if (hasSpawned) return;
        SpawnPersistentObjects();
        hasSpawned = true;
    }

    void SpawnPersistentObjects() {
        GameObject persistentObject = Instantiate(persistentObjectPrefab);
        DontDestroyOnLoad(persistentObject);
    }
}
