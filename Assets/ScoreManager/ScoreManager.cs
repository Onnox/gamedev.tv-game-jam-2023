using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScoreManager: MonoBehaviour {

    private int dotScore = 0;
    public int DotScore { get { return dotScore; } }
    private int dashScore = 0;
    public int DashScore { get { return dashScore; } }
    public int BalanceScore { get { return Mathf.Min(dotScore, dashScore); } }

    public int TotalScore { get { return dotScore + dashScore + BalanceScore; } }

    public event Action OnDotScoreChanged;
    public event Action OnDashScoreChanged;

    public void AddPoints(int points, ScoreType scoreType) {
        switch (scoreType) {
            case ScoreType.Dot:
                dotScore += points;
                OnDotScoreChanged?.Invoke();
                break;
            case ScoreType.Dash:
                dashScore += points;
                OnDashScoreChanged?.Invoke();
                break;
        }
    }
}
