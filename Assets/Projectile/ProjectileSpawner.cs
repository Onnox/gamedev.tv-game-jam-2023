using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using System;

public class ProjectileSpawner: MonoBehaviour {
    
    [SerializeField] private Transform projectileSpawnLocation;
    [SerializeField] private ShootingPowerHandler shootingPowerHandler;
    [SerializeField] private AmmoHandler ammoLimit;
    [Space]
    [SerializeField] private Projectile projectilePrefab;
    [SerializeField] private float maxForce = 100f;

    private IObjectPool<Projectile> projectilePool;

    public event Action OnLastProjectileDestroyed;

    private void OnDestroy() {
        shootingPowerHandler.OnShoot -= SpawnProjectile;
    }

    private void Awake() {
        projectilePool = new ObjectPool<Projectile>(
            OnCreateProjectile,
            OnGetProjectile,
            OnReleaseProjectile,
            OnDestroyProjectile
        );
        shootingPowerHandler.OnShoot += SpawnProjectile;
    }

    private void SpawnProjectile() {
        Projectile projectile = projectilePool.Get();
        projectile.transform.position = projectileSpawnLocation.position;
        projectile.ProjectileRigidbody.AddForce(projectileSpawnLocation.forward * shootingPowerHandler.CurrentPower * maxForce);
    }

    private void CheckIfLastProjectileReleased() {
        if (ammoLimit.CurrentAmmo == 0 && CountLivingProjectiles() == 0) {
            OnLastProjectileDestroyed?.Invoke();
        }
    }

    private int CountLivingProjectiles() {
        int numberOfActiveChildren = 0;
        foreach (Transform child in transform) {
            if (child.gameObject.activeInHierarchy) {
                numberOfActiveChildren++;
            }
        }
        return numberOfActiveChildren;
    }

    #region ObjectPool methods

    Projectile OnCreateProjectile() {
        Projectile projectile = Instantiate(projectilePrefab, transform);
        projectile.Pool = projectilePool;
        return projectile;
	}

    void OnGetProjectile(Projectile projectile) {
        projectile.gameObject.SetActive(true);
	}

    void OnReleaseProjectile(Projectile projectile) {
        projectile.gameObject.SetActive(false);
        CheckIfLastProjectileReleased();
	}

    void OnDestroyProjectile(Projectile projectile) {
        Destroy(projectile.gameObject);
	}

    #endregion
}
