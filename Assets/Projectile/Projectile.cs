using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Projectile: MonoBehaviour {
    
	[SerializeField] private float deathDuration = 1.25f;
	[SerializeField] private MeshRenderer meshRenderer;
	[SerializeField] private ParticleSystem explosionParticles;
	[SerializeField] private ParticleSystem flightParticles;

    private AudioSource deathAudioSource;
    private Collider projectileCollider;
	private Rigidbody projectileRigidbody;
	public Rigidbody ProjectileRigidbody { get { return projectileRigidbody; } }

    private IObjectPool<Projectile> pool;
	public IObjectPool<Projectile> Pool { set { pool = value; } }

	private void Awake() {
        deathAudioSource = GetComponent<AudioSource>();
        projectileCollider = GetComponent<Collider>();
		projectileRigidbody = GetComponent<Rigidbody>();
	}

	private void OnEnable() {
		projectileRigidbody.isKinematic = false;
		projectileCollider.enabled = true;
		meshRenderer.enabled = true;
		flightParticles.Play();
	}

	private void OnCollisionEnter(Collision other) {
		Target target = other.gameObject.GetComponentInParent<Target>();
		if (target != null) {
			target.HandleHit();
		}
		HandleHit();
	}

	private void HandleHit() {
		projectileRigidbody.isKinematic = true;
		projectileCollider.enabled = false;
		StartCoroutine(DoHitAnimation());
	}

	private IEnumerator DoHitAnimation() {
        deathAudioSource.Play();
        meshRenderer.enabled = false;
		flightParticles.Stop();
		explosionParticles.Play();
		yield return new WaitForSeconds(deathDuration);
		pool.Release(this);
	}
}
