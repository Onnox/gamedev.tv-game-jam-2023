using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Target: MonoBehaviour {

    [Header("Collision and Hiding")]
    [SerializeField] Collider targetCollider;
    [SerializeField] MeshRenderer[] meshRenderers;
    [Header("Scoring Settings")]
    [SerializeField] private int pointsValue = 100;
    public int PointsValue { get { return pointsValue; } }
    [SerializeField] private ScoreType scoreType;
    [Header("Animation Settings")]
    [SerializeField] private float spawnAnimationDuration = 1f;
    [SerializeField] private float despawnAnimationDuration = 1f;
    [SerializeField] private float hitAnimationDuration = 1f;
    [Header("Lifetime Settings")]
    private float minUpDuration = 3f;
    private float maxUpDuration = 10f;
    private float minDownDuration = 3f;
    private float maxDownDuration = 20f;

    private ScoreManager scoreManager;
    private System.Random rng;
    private Animator animator;
    private TargetAudio targetAudio;

    private void Awake() {
        scoreManager = GameObject
            .FindWithTag(Constants.Tags.scoreManager)
            .GetComponent<ScoreManager>();
        rng = new System.Random();
        animator = GetComponentInChildren<Animator>();
        targetAudio = GetComponent<TargetAudio>();
    }

    private void Start() {
        StartSpawnCountdown();
    }

    private void StartSpawnCountdown() {
        float downTime = rng.RandomFloat(minDownDuration, maxDownDuration);
        Debug.Log($"{name} will spawn in {downTime} seconds");
        StartCoroutine(SpawnCountdown(downTime));
    }

    private IEnumerator SpawnCountdown(float seconds) {
        yield return new WaitForSeconds(seconds);
        StartSpawn();
    }

    private void StartSpawn() {
        targetCollider.enabled = true;
        foreach (Renderer renderer in meshRenderers) {
            renderer.enabled = true;
        }
        StartCoroutine(DoSpawnAnimation());
    }

    private IEnumerator DoSpawnAnimation() {
        targetAudio.PlaySpawnClip();
        animator.SetTrigger(Constants.AnimatorParams.targetActivateTrigger);
        yield return new WaitForSeconds(spawnAnimationDuration);
        float lifetime = rng.RandomFloat(minUpDuration, maxUpDuration);
        Debug.Log($"Spawned {name} with lifetime of {lifetime} seconds");
        yield return StartCoroutine(DespawnCountdown(lifetime));
    }

    private IEnumerator DespawnCountdown(float seconds) {
        yield return new WaitForSeconds(seconds);
        yield return StartCoroutine(DoDespawnAnimation());
    }

    public void HandleHit() {
        StopAllCoroutines();
        targetCollider.enabled = false;
        scoreManager.AddPoints(pointsValue, scoreType);
        StartCoroutine(DoHitAnimation());
    }

    private IEnumerator DoHitAnimation() {
        targetAudio.PlayHitClip();
        animator.SetTrigger(Constants.AnimatorParams.targetHitTrigger);
        yield return new WaitForSeconds(hitAnimationDuration);
        foreach (Renderer renderer in meshRenderers) {
            renderer.enabled = false;
        }
        StartSpawnCountdown();
    }

    private IEnumerator DoDespawnAnimation() {
        targetAudio.PlayDespawnClip();
        animator.SetTrigger(Constants.AnimatorParams.targetDeactivateTrigger);
        targetCollider.enabled = false;
        yield return new WaitForSeconds(despawnAnimationDuration);
        foreach (Renderer renderer in meshRenderers) {
            renderer.enabled = false;
        }
        StartSpawnCountdown();
    }
}
