using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetAudio : MonoBehaviour {

    [SerializeField] private AudioClip hitClip;
    [SerializeField] private AudioClip spawnClip;
    [SerializeField] private AudioClip despawnClip;

    private AudioSource audioSource;

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayHitClip() {
        audioSource.Stop();
        audioSource.PlayOneShot(hitClip);
    }

    public void PlaySpawnClip() {
        audioSource.Stop();
        audioSource.PlayOneShot(spawnClip);
    }

    public void PlayDespawnClip() {
        audioSource.Stop();
        audioSource.PlayOneShot(despawnClip);
    }
}
