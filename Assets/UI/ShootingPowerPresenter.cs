using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootingPowerPresenter: MonoBehaviour {
    
    [SerializeField] private ShootingPowerHandler shootingPowerHandler;
    [SerializeField] private Image barFillImage;

    private void Start() {
        shootingPowerHandler.OnPowerChanged += UpdateUI;
        UpdateUI();
    }

    private void UpdateUI() {
        barFillImage.fillAmount = shootingPowerHandler.CurrentPower;
    }
}
