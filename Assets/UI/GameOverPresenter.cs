using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPresenter: MonoBehaviour {

    [SerializeField] private ProjectileSpawner projectileSpawner;
    [SerializeField] private CanvasFadeInOut gameOverCanvasFadeInOut;
    [SerializeField] private AudioSource gameOverAudioSource;

    private void Start() {
        projectileSpawner.OnLastProjectileDestroyed += ShowGameOver;
    }

    private void ShowGameOver() {
        Time.timeScale = 0f;
        gameOverCanvasFadeInOut.StartFadeIn();
        gameOverAudioSource.Play();
    }
}
