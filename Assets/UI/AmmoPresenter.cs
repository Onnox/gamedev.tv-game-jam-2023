using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AmmoPresenter : MonoBehaviour {

    [SerializeField] private AmmoHandler ammoHandler;
    [Header("UI")]
    [SerializeField] private Transform ammoHStack;
    [SerializeField] private GameObject ammoImagePrefab;

    private void Start() {
        ammoHandler.OnCurrentAmmoChanged += UpdateUI;
        UpdateUI();
    }

    private void UpdateUI() {
        // This is horribly inefficient. I wouldn't do this outside the time constraints of a game jam!!!
		foreach (Transform transform in ammoHStack.transform) {
            Destroy(transform.gameObject);
		}

		for (int i = 0; i < ammoHandler.CurrentAmmo; i++) {
            Instantiate(ammoImagePrefab, ammoHStack);
		}
    }
}
