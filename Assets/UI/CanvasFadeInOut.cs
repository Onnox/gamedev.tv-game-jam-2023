using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasFadeInOut : MonoBehaviour
{

    const float defaultCanvasFadeSpeed = 2f;

    CanvasGroup canvasGroup;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void StartFadeIn(float fadeSpeed = defaultCanvasFadeSpeed)
    {
        StartFadeIn(fadeSpeed, null);
    }

    public void StartFadeIn(float fadeSpeed, Action onCompleted)
    {

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }

        StopAllCoroutines();
        StartCoroutine(FadeInCanvas(fadeSpeed, onCompleted));
    }

    IEnumerator FadeInCanvas(float fadeSpeed, Action onCompleted)
    {

        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += fadeSpeed * Time.unscaledDeltaTime;
            yield return new WaitForEndOfFrame();
        }

        onCompleted?.Invoke();
    }

    public void StartFadeOut(float fadeSpeed = defaultCanvasFadeSpeed)
    {
        StartFadeOut(fadeSpeed, null);
    }

    public void StartFadeOut(float fadeSpeed, Action onCompleted)
    {
        StopAllCoroutines();
        StartCoroutine(FadeOutCanvas(fadeSpeed, onCompleted));
    }

    IEnumerator FadeOutCanvas(float fadeSpeed, Action onCompleted)
    {

        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= fadeSpeed * Time.unscaledDeltaTime;
            yield return new WaitForEndOfFrame();
        }

        FinishFadeOut(onCompleted: onCompleted);
    }

    protected virtual void FinishFadeOut(Action onCompleted = null)
    {

        canvasGroup.alpha = 0;

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }

        onCompleted?.Invoke();
    }
}
