using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScorePresenter: MonoBehaviour {

    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private TMP_Text dotScoreText;
    [SerializeField] private TMP_Text dashScoreText;
    [SerializeField] private TMP_Text gameOverDotScoreText;
    [SerializeField] private TMP_Text gameOverDashScoreText;
    [SerializeField] private TMP_Text gameOverBonusScoreText;
    [SerializeField] private TMP_Text gameOverTotalScoreText;
    [Space]
    [SerializeField] private Animator dotScoreAnimator;
    [SerializeField] private Animator dashScoreAnimator;

    private void OnDestroy() {
        scoreManager.OnDotScoreChanged -= UpdateDotScoreUI;
        scoreManager.OnDashScoreChanged -= UpdateDashScoreUI;
    }

    private void Start() {
        scoreManager.OnDotScoreChanged += UpdateDotScoreUI;
        scoreManager.OnDashScoreChanged += UpdateDashScoreUI;
    }

    private void UpdateDotScoreUI() {
        dotScoreText.text = $"{scoreManager.DotScore}";
        UpdateGameOverScoreUI();
        dotScoreAnimator.SetTrigger(Constants.AnimatorParams.pingTrigger);
    }

    private void UpdateDashScoreUI() {
        dashScoreText.text = $"{scoreManager.DashScore}";
        UpdateGameOverScoreUI();
        dashScoreAnimator.SetTrigger(Constants.AnimatorParams.pingTrigger);
    }

    private void UpdateGameOverScoreUI() {
        gameOverDotScoreText.text = $"{scoreManager.DotScore}";
        gameOverDashScoreText.text = $"{scoreManager.DashScore}";
        gameOverBonusScoreText.text = $"Balance bonus: {scoreManager.BalanceScore}";
        gameOverTotalScoreText.text = $"{scoreManager.TotalScore}";
    }
}
