using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Constants {
    
    public struct Layers {
        public const string targetLayer = "TargetLayer";
    }

    public struct Tags {
        public const string scoreManager = "ScoreManager";
    }

    public struct AnimatorParams {
        public const string targetHitTrigger = "targetHitTrigger";
        public const string targetActivateTrigger = "targetActivateTrigger";
        public const string targetDeactivateTrigger = "targetDeactivateTrigger";
        public const string pingTrigger = "pingTrigger";
    }
}
